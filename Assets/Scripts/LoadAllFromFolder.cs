﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using System.IO;

public class LoadAllFromFolder : MonoBehaviour
{

    public string filesLocation = @"C:\Test";
    public List<Texture2D> images = new List<Texture2D>();
    public GameObject objSprite;

    public IEnumerator Start()
    {
        yield return StartCoroutine(
            "LoadAll",
            Directory.GetFiles(filesLocation, "*.png", SearchOption.AllDirectories)

        );
    }

    public IEnumerator LoadAll(string[] filePaths)
    {
        foreach (string filePath in filePaths)
        {
            WWW load = new WWW("file:///" + filePath);
            yield return load;
            if (!string.IsNullOrEmpty(load.error))
            {
                Debug.LogWarning(filePath + " error");
            }
            else
            {
                images.Add(load.texture);
                objSprite.GetComponent<SpriteRenderer>().sprite = Sprite.Create(images[0], new Rect(0, 0, images[0].width, images[0].height), new Vector2(0.5f, 0.5f), 100);
            }
        }
    }
}