﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections.Generic;
using System.Collections;

public class FindImages : MonoBehaviour {

    public Sprite sp;
    public List<Sprite> enumSp;

    public List<Texture2D> Textures;
    public List<Sprite> Sprites;
    bool LoadComplete = false;

    public GameObject listGO;

    public IEnumerator StartLoad(string path)
    {
        

        var Location = Application.dataPath + "/StreamingAssets/" + path;  //récupération des tetes

        yield return StartCoroutine("Load", Directory.GetFiles(Location, "*.png", SearchOption.AllDirectories)); // lancer les recherches de sprites

        //LoadComplete = true;
    }

    public IEnumerator Load(string[] filePaths)
    {
        foreach (string filePath in filePaths)
        {
            WWW load = new WWW("file:///" + filePath);
            yield return load;
            if (!string.IsNullOrEmpty(load.error))
            {
                Debug.LogWarning(filePath + " error");
            }
            else
            {
                Textures.Add(load.texture);
            }
        }

        Updater();
    } // charger les tetes

    void Update()
    {
        if (LoadComplete == true)
        {
            Updater();
            LoadComplete = false;
        }

    }

    public void Updater()// refresh head
    {
        foreach(Texture2D tx2D in Textures)
        {
            Sprites.Add(Sprite.Create(tx2D, new Rect(0, 0, tx2D.width, tx2D.height), new Vector2(0.5f, 0.5f)));
            Debug.Log(tx2D);
        }

        Return(); 

        //Sprites.Add(Sprite.Create(Textures[0], new Rect(0, 0, Textures[0].width, Textures[0].height), new Vector2(0.5f, 0.5f)));
        //Debug.Log(Sprites[0]);
        //HeadSlot.GetComponent<Image>().sprite = Sprites;
    }



    public List<Sprite> CallLoad(string path, GameObject caller)
    {
        Textures.Clear();
        Sprites.Clear();

        listGO = caller;
        StartCoroutine("StartLoad", path);

        return Sprites;
    }


    public void Return()
    {
        listGO.GetComponent<CreateSelectList>().SetElements(Sprites);
    }








































    /*void Start () {
        //Initialize();

        //Debug.Log(Application.persistentDataPath + "/Sprites");

        
    }

    public List<Sprite> CallLoad(string path)
    {
        enumSp = null;

        var location = Application.dataPath + "/StreamingAssets/" + path;
        //StartCoroutine("Load", Directory.GetFiles(location, "*.png", SearchOption.AllDirectories)); // lancer les recherches de sprites

        var filePaths = Directory.GetFiles(location, "*.png", SearchOption.AllDirectories);

        foreach (string filePath in filePaths)
        {

            WWW load = new WWW("file:///" + filePath);
            Debug.Log(load);
            if (!string.IsNullOrEmpty(load.error))
            {
                Debug.LogWarning(filePath + " error");
            }
            else
            {
                enumSp.Add(Sprite.Create(load.texture, new Rect(0, 0, load.texture.width, load.texture.height), new Vector2(0.5f, 0.5f)));
                Debug.Log(filePath);
            }

            enumSp.Add(Sprite.Create(load.texture, new Rect(0, 0, load.texture.width, load.texture.height), new Vector2(0.5f, 0.5f)));
            Debug.Log(filePath);
        }

        System.Diagnostics.Process.Start("explorer.exe", location);

        return enumSp;
    }

    public IEnumerator Load(string[] filePaths)
    {
        
        foreach (string filePath in filePaths)
        {
            
            WWW load = new WWW("file:///" + filePath);
            Debug.Log(load);
            yield return load;
            if (!string.IsNullOrEmpty(load.error))
            {
                Debug.LogWarning(filePath + " error");
            }
            else
            {
                enumSp.Add(Sprite.Create(load.texture, new Rect(0, 0, load.texture.width, load.texture.height), new Vector2(0.5f, 0.5f)));
                Debug.Log(filePath);
            }
        }
    } // charger les tetes

    public List<Sprite> Initialize(string path)
    {
        List<Sprite> sps = new List<Sprite>();

        //string itemPath = Application.persistentDataPath + "/Sprites";
        //itemPath = itemPath.Replace(@"/", @"\");   // explorer doesn't like front slashes
        path = Application.dataPath + "/StreamingAssets/" + path;
        path = path.Replace(@"/", @"\");   // explorer doesn't like front slashes
        DirectoryInfo directoryInformation = new DirectoryInfo(path);
        if (!directoryInformation.Exists)
            directoryInformation.Create();

        FileInfo[] fileList = directoryInformation.GetFiles();

        foreach (FileInfo file in fileList)
        {
            if (file.Extension == ".png")
            {
                Debug.Log(file);

                sps.Add(LoadSprite(file.ToString()));


            }
        }

        GetComponent<Image>().sprite = sp;

        System.Diagnostics.Process.Start("explorer.exe", path);
        return sps;
    }

    public void OpenFile()
    {
        //string itemPath = Application.persistentDataPath + "/Sprites";
        //itemPath = itemPath.Replace(@"/", @"\");   // explorer doesn't like front slashes
        System.Diagnostics.Process.Start("explorer.exe", "/select," + "Body");
    }

    private Sprite LoadSprite(string path)
    {
        if (string.IsNullOrEmpty(path)) return null;
        if (System.IO.File.Exists(path))
        {
            byte[] bytes = System.IO.File.ReadAllBytes(path);
            Texture2D texture = new Texture2D(1, 1);
            texture.LoadImage(bytes);
            Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            return sprite;
        }
        return null;
    }*/
}