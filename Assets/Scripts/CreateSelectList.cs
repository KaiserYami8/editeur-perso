﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections.Generic;

public class CreateSelectList : MonoBehaviour {

    [Header("Buttons List")]
    public GameObject[] buttonsList;

    [Header("Elements List")]
    public string path;
    public string targetName;
    public GameObject[] elementsList;
    public GameObject backHUD;
    List<Sprite> sps = new List<Sprite>();

    public void SetButtons () {
        if (buttonsList.Length > 0)
        {
            //Set Content
            GameObject content = GameObject.FindGameObjectWithTag("Content");

            //Save childs
            Transform[] childs;
            childs = new Transform[content.transform.childCount];
            for (int i = 0; i < childs.Length; i++)
            {
                childs[i] = content.transform.GetChild(i);
            }

            //Instantiate all buttons
            for (int i = 0; i < buttonsList.Length; i++)
            {
                Debug.Log(buttonsList[i]);
                var go = Instantiate(buttonsList[i], content.transform);
                go.GetComponent<RectTransform>().anchoredPosition = new Vector2(50, -50 + -120 * i);
            }

            //Resize Content
            content.GetComponent<RectTransform>().sizeDelta = new Vector2(0, buttonsList.Length * 120);

            //Destroy all olds
            foreach (Transform child in childs)
            {
                Destroy(child.gameObject);
            }
        }
	}

    public void SetList()
    {
        FindObjectOfType<FindImages>().CallLoad(path, gameObject);
        Debug.Log(sps);
    }

    public void SetElements(List<Sprite> list)
    {
        if (elementsList.Length != 110)
        {
            //Set Content
            GameObject content = GameObject.FindGameObjectWithTag("ContentElements");

            //Save childs
            Transform[] childs;
            childs = new Transform[content.transform.childCount];
            for (int i = 0; i < childs.Length; i++)
            {
                childs[i] = content.transform.GetChild(i);
            }

            sps = list;
            Debug.Log(sps);

            for (int i = 0; i < sps.Count; i ++)
            {
                GameObject go = Instantiate(backHUD, content.transform);
                go.GetComponent<RectTransform>().anchoredPosition = new Vector2(150 + 200 * (i % 3), -150 + -200 * ((i - i % 3) / 3));

                go.transform.GetChild(0).GetComponent<Image>().sprite = sps[i];

                go.GetComponent<HUDButton>().target = GameObject.Find(targetName).transform.GetChild(0).gameObject;
            }

            content.GetComponent<RectTransform>().sizeDelta = new Vector2(content.GetComponent<RectTransform>().sizeDelta.x, Mathf.Ceil(sps.Count / 3));

            //Destroy all olds
            foreach (Transform child in childs)
            {
                Destroy(child.gameObject);
            }
        }
    }
}
