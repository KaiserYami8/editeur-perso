﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class HUDScroller : MonoBehaviour {

    public List<Sprite> images;
    public int imgIndex;
    public int slideDir;
    public float xPos;
    public float distance;

    public GameObject imgShown, imgLeft, imgRight;
    
	void Start () {
        //images = FindObjectOfType<FindImages>().Initialize("Body");
        distance = Vector2.Distance(imgShown.GetComponent<RectTransform>().anchoredPosition, imgRight.GetComponent<RectTransform>().anchoredPosition);
	}

    public void Refresh()
    {
        images = new List<Sprite>();
        //images = FindObjectOfType<FindImages>().Initialize("Body");
    }
	
	void Update () {
        xPos = Mathf.Lerp(xPos, slideDir * distance, .2f);
        imgLeft.GetComponent<RectTransform>().anchoredPosition = new Vector2(xPos - distance, 0);
        imgShown.GetComponent<RectTransform>().anchoredPosition = new Vector2(xPos, 0);
        imgRight.GetComponent<RectTransform>().anchoredPosition = new Vector2(xPos + distance, 0);

        if (Mathf.Abs(xPos) / distance >= .99f)
        {
            if (slideDir == -1)
                imgShown.GetComponent<Image>().sprite = imgRight.GetComponent<Image>().sprite;
            if (slideDir == 1)
                imgShown.GetComponent<Image>().sprite = imgLeft.GetComponent<Image>().sprite;

            xPos = slideDir = 0;
        }
    }

    public void NextImage(int ind)
    {
        if (xPos == 0)
        {
            imgIndex += ind;
            slideDir = ind;

            if (imgIndex == -1) imgIndex = images.Count - 1;
            if (imgIndex == images.Count) imgIndex = 0;

            if (ind == 1)
                imgLeft.GetComponent<Image>().sprite = images[imgIndex];
            if (ind == -1)
                imgRight.GetComponent<Image>().sprite = images[imgIndex];
        }
    }
}