﻿using UnityEngine;

public class MainCamera : MonoBehaviour {

    public float size;
    public Vector2 pos;

    Camera cam;
    
	void Start () {
        cam = GetComponent<Camera>();
	}
	
	void Update () {
        transform.position = Vector2.Lerp(transform.position, pos, .2f);
        cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, size, .2f);
	}
}
