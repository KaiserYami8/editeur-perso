﻿using UnityEngine;

public class HUDWindows : MonoBehaviour {

    public GameObject Windows;

    bool isFullView;

    public void ChangeWindow(int i)
    {
        foreach (Transform child in Windows.transform)
        {
            child.GetComponent<CanvasGroup>().alpha = 0;
            child.GetComponent<CanvasGroup>().blocksRaycasts = false;
            child.GetComponent<CanvasGroup>().interactable = false;
        }

        Windows.transform.GetChild(i).GetComponent<CanvasGroup>().alpha = 1;
        Windows.transform.GetChild(i).GetComponent<CanvasGroup>().blocksRaycasts = true;
        Windows.transform.GetChild(i).GetComponent<CanvasGroup>().interactable = true;
    }

    public void MoveCam(bool fullView)
    {
        isFullView = fullView;
        SetCamVar();
    }

    void SetCamVar()
    {
        if (isFullView)
        {
            FindObjectOfType<MainCamera>().pos = new Vector2(0, 0);
            FindObjectOfType<MainCamera>().size = 5;
        }
        else
        {
            FindObjectOfType<MainCamera>().pos = new Vector2(-1.8f, 2.5f);
            FindObjectOfType<MainCamera>().size = 2;
        }
    }
}